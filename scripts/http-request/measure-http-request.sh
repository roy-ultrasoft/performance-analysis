# @see https://ec.haxx.se/usingcurl-verbose.html#available---write-out-variables

URL=$1
NOW=$(date +%Y-%m-%dT%H:%M:%S)
TABLE_NAME="LOG_HTTP_REQUEST"
curl -s -w"@curl-format.txt" $URL | sed 's/\$NOW/'$NOW'/g' | sed 's/\$TABLE_NAME/'$TABLE_NAME'/g'
