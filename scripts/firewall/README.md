# Firewall

## things to try:

### second port with scalable firewall

#### targets
- we can prove that the problems are related to a specific firewall rule/setting.
- maybe the problem only occurs in a specific combination of firewall rules.
- maybe there is a delay in the JVM

#### on network firewall
- disable all restrictions on an uncommon port (4040)

#### application server
- bind the new port to the existing Apache Web Server ([as second port (Apache 2.4)](https://httpd.apache.org/docs/2.4/bind.html))
- log every request duration to a specific (dummy servlet)[https://www.ntu.edu.sg/home/ehchua/programming/java/JavaServletExamples.html] and a static html page in a dummy WebApp (don't forget the JasperReportServer)

#### client
- measure the difference between these two ports by executing the [measure-http-request.sh](https://bitbucket.org/roy-ultrasoft/performance-analysis/src/master/scripts/http-request/) script for a while

#### after a while
- after every run evaluate the delta (difference between client and server processing durations, html and servlet) the server and client results then you compare the delta to the previous delta.
- compare the results with reference system, at best with the same firewall rules => Docker, other Customer ...
- try binary search enabling the firewall rules. May we will detect a bottleneck
