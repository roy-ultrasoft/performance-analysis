# Analyze Disk IO

## Linux

### full disk
  [Documentation: /proc/diskstats output](https://www.kernel.org/doc/Documentation/ABI/testing/procfs-diskstats)
  [Documentation: /proc/diskstats full](https://www.kernel.org/doc/Documentation/iostats.txt)

```sh
1. find device name:
mount | grep " on / "

Output:
/dev/nvme0n1p1 on / type ext4 (rw,relatime,errors=remount-ro,data=ordered)

2. record data for 5 min
timeout 300 bash -c "while sleep 1; do grep nvme0n1p1 /proc/diskstats; done" &> /tmp/out
```

### single process

  [Documentation: /proc](https://www.kernel.org/doc/Documentation/filesystems/proc.txt) => search for: "3.3 /proc/<pid>/io"
```sh
cat /proc/$PID/io

timeout 300 bash -c "while sleep 1; do cat /proc/"$PID"/io; done" &> /tmp/out
```
## Windows

### full disk
like "single process", just without the "where" expression ???

### single process
  [Source: Stackoverflow](https://stackoverflow.com/questions/26761906/how-to-get-i-o-reads-from-a-specified-task-in-the-tasklist-into-a-bat-file-varia)
  [Documentation: WMIC](https://www.cs.cmu.edu/~tgp/scsadmins/winadmin/WMIC_Queries.txt)

```cmd
@echo off
wmic process where name="java" get readoperationcount /format:csv>tempuni.txt
type tempuni.txt>temp.txt
del tempuni.txt
setLocal EnableDelayedExpansion
for /f "skip=2 tokens=2 delims=, " %%a in (temp.txt) do (
    set /a N+=1
    set v%N%=%%a
)
del temp.txt
set IOREADS=%v2%
echo I/O Reads = %IOREADS%
```
