echo "CPU Test Started"
CORES=2
TABLE_NAME="LOG_CPU_SPEED"
ALG=rsa
START=$(date +%Y-%m-%dT%H:%M:%S)
openssl speed -multi $CORES $ALG > /dev/null 2>&1
END=$(date +%Y-%m-%dT%H:%M:%S)
echo "INSERT INTO ${TABLE_NAME} (\r
    start,\r
    end, \r
    cores, \r
    algorythm\r)\r
VALUES\r
    ('${START}',\r
    '${END}',\r
    '${CORES}',
    '${ALG}');"
