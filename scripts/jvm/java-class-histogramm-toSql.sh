# jcmd $PID to list available commands
PID=$1
TABLE_NAME="LOG_CLASS_HISTOGRAM"
jcmd $PID GC.class_histogram | head | awk '{if($2 && !match($1, "num|--")) { print "INSERT INTO '$TABLE_NAME' (instances, bytes, class_name) VALUES ("$2 "," $3 ",\x27" $4"\x27);" ;}}' 2>&1
