# performance-analysis

## Configs to check

## Compress HTTP responses at the Tomcat level
Add the following attributes to the connector (compression, compressMinSize, noCompressionUserAgents and compressableMimeType) as in the example below:
```xml
<Connector compressableMimeType="text/html,text/xml,text/css,text/javascript, application/x-javascript,application/javascript"
compression="on"
compressionMinSize="128"
connectionTimeout="20000"
noCompressionUserAgents="gozilla, traviata"
port="8080"
redirectPort="8443" protocol="HTTP/1.1" />
```
## Database Pooling
- Tune the number of connections in the database pool **=> check default values for differrent tomcat versions !!!**
```xml
<Resource name="jdbc/TestDB"
          auth="Container"
          type="javax.sql.DataSource"
          factory="org.apache.tomcat.jdbc.pool.DataSourceFactory"
          testWhileIdle="true"
          testOnBorrow="true"
          testOnReturn="false"
          validationQuery="SELECT 1"
          validationInterval="30000"
          timeBetweenEvictionRunsMillis="30000"
          maxActive="100"
          minIdle="10"
          maxWait="10000"
          initialSize="10"
          removeAbandonedTimeout="60"
          removeAbandoned="true"
          logAbandoned="true"
          minEvictableIdleTimeMillis="30000"
          jmxEnabled="true"
          jdbcInterceptors="org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;
            org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer"
          username="root"
          password="password"
          driverClassName="com.mysql.jdbc.Driver"
          url="jdbc:mysql://localhost:3306/mysql"/>
```
  - **maxActive:** Maximum number of dB connections in pool. Make sure your max_connections is large enough (0 for no limit)
  - **maxIdle:** Maximum number of idle dB connections to retain in pool (-1 for no limit)
    - if you have 100 max Active connections and say you set maxIdle to 80. Assuming there are no requests going to the database, only 80 connections will be tested (via the validationquery = **TestsPerEvictionRun**) and will stay active. The other 20 will be closed
  - **maxWait:** Maximum time to wait for a dB connection to become available in ms, in this example 10 seconds. **An Exception is thrown** if this timeout is exceeded (-1 to wait indefinitely)

  - **timeBetweenEvictionRunsMillis:** The number of milliseconds to sleep between runs of the idle object evictor thread. When non-positive, no idle object evictor thread will be run.
  - **numTestsPerEvictionRun:** The number of objects to examine during each run of the idle object evictor thread.
  - **minEvictableIdleTimeMillis:** The minimum amount of time an object may sit idle in the pool before it is eligible for eviction by the idle object evictor.